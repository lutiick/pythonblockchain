from django.db import models


class Block(models.Model):
    """Модель бля блока"""
    height = models.IntegerField('Высота блока', primary_key=True)
    hash = models.CharField('Хэш блока', max_length=64)
    timestamp = models.DateTimeField('Таймштамп блока')
    miner_address = models.CharField('Адресс майнера', max_length=34)
    tx_count = models.IntegerField('Число транзакций в блоке')

    def __str__(self):
        return f'Блок - высота ({self.height}), хэш ({self.hash})'

    class Meta:
        verbose_name = "Блок"
        verbose_name_plural = "Блоки"

from django.shortcuts import render

from django.views import generic
from core.models import Block


class IndexView(generic.ListView):
    """Главная страница, список всех блоков"""
    paginate_by = 50
    model = Block
    template_name = 'core/index.html'
    context_object_name = 'blocks'

    def get_queryset(self):
        return Block.objects.order_by('-height')


class DetailView(generic.DetailView):
    """Подробная страница для блоков"""
    model = Block
    template_name = 'core/detail.html'
    context_object_name = 'block_ob'

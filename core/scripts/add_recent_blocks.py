from core.models import Block
import requests
from datetime import datetime
import sys
from django.db.utils import IntegrityError


def run(count):
    """Скрипт для добавления count недавний блоков в бд
        Использование: manage.py runscript add_recent_blocks count"""
    if int(count) <= 0:
        return
    url = f'https://bcschain.info/api/recent-blocks?count={count}'
    r = requests.get(url)
    data = r.json()
    blocks = []
    for block in data:
        blocks.append(
            Block(
                height=block['height'],
                hash=block['hash'],
                timestamp=datetime.fromtimestamp(block['timestamp']),
                miner_address=block['miner'],
                tx_count=block['transactionCount']
            )
        )
    try:
        Block.objects.bulk_create(blocks)
    except IntegrityError as e:
        return
from background_task import background
from .models import Block
import requests
from datetime import datetime
from background_task.models import Task


@background(schedule=0)
def load_new_blocks():
    """Фоновая задача для добавления новых блоков в базу данных через апи
       Запускается в отдельной консоли
       Запуск: manage.py process_tasks"""
    last_block_height = Block.objects.latest('height').height
    while True:
        last_block_height += 1
        r = requests.get(f'https://bcschain.info/api/block/{last_block_height}')
        if r.status_code == 404:
            return
        data = r.json()
        b = Block(height=data['height'],
                  hash=data['hash'],
                  timestamp=datetime.fromtimestamp(data['timestamp']),
                  miner_address=data['miner'],
                  tx_count=len(data['transactions'])
                  )
        b.save()
        print("Block " + str(data['height']) + 'added')


Task.objects.all().delete()
load_new_blocks(repeat=60)
